import { Component, OnInit } from '@angular/core';
import { RequestOptions, Request, RequestMethod} from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-dec',
  templateUrl: './dec.page.html',
  styleUrls: ['./dec.page.scss'],
})
export class DecPage implements OnInit {

    switchToBase64String = "Switch to Base64";
    switchToHexString = "Switch to Hex";
    
    aesKeyValue: string;
    payloadValue: string;
    finalAesKeyValue: string;
    finalPayloadValue: string;
    aesKeyFormat: string;
    payloadFormat: string;
    result;
    statusLabel : string;
    
    constructor(private http: HttpClient) {
        this.switchToBase64String = "Switch to Base64";
        this.switchToHexString = "Switch to Hex";
    }

    
    base64ToHex(base64String) {
          var raw = atob(base64String);

          var HEX = '';

          for (var i = 0; i < raw.length; i++ ) {

            var _hex = raw.charCodeAt(i).toString(16)

            HEX += (_hex.length==2?_hex:'0'+_hex);

          }
          return HEX.toUpperCase();
    }
    
    hexToBase64(hexString) {
        return btoa(hexString.match(/\w{2}/g).map(function(a) {
            return String.fromCharCode(parseInt(a, 16));
        }).join(""));
    }
    
    switchFormat(switchBtnID) {
        try {
                console.log("SwitchFormat clicked " + switchBtnID);
                this.statusLabel = "";
                var switchBtn = document.getElementById(switchBtnID + "Switch");
                var inputField = (<HTMLInputElement>document.getElementById(switchBtnID + "Input"));
                var currString = inputField.value;
                console.log("switchBtn = " + switchBtn + "inputField = " + inputField + " string = " + currString);
                console.log("swtichBtnInnerText = " + switchBtn.innerText + " sw = " + this.switchToBase64String.toUpperCase());
                if (switchBtn.innerText == this.switchToBase64String.toUpperCase()) {
//                console.log("converting from hex to base 64")
                    if (currString != "") {
                        inputField.value = this.hexToBase64(currString);
                    }  
                    switchBtn.innerText = this.switchToHexString;
                } else {
                    if (currString != "") {
                        inputField.value = this.base64ToHex(currString);
                    } 
                    switchBtn.innerText = this.switchToBase64String;
                }
        } catch (e) {
            if (e instanceof TypeError) {
                this.statusLabel = switchBtnID + " Field is currently invalid, please enter a valid value before converting";
            } else if (e instanceof DOMException) {
                this.statusLabel = "Invalid encoding, please enter a valid value";
            } else {
                console.error(e);
            }
        }
    }

    ngOnInit() {
        console.log("Hi2");
            
        
        
        
        this.aesKeyValue = sessionStorage.getItem("aesKey");
        this.payloadValue = sessionStorage.getItem("payload");
        
        sessionStorage.removeItem("aesKey");
        sessionStorage.removeItem("payload");
//        sessionStorage.removeItem("aesKeyFormat");
//        sessionStorage.removeItem("payloadFormat");
        
    }
    
    ngAfterContentInit() {

    setTimeout(() => {
        if (sessionStorage.getItem("aesKeyFormat") == "Base64") {
//            console.log("Here3 " + this.switchToHexString);
            document.getElementById("aesKeySwitch").innerText = this.switchToHexString;
//            console.log("Here4 " + document.getElementById("aesKeySwitch").innerText);
        }
    
        if (sessionStorage.getItem("payloadFormat") == "Base64") {
//            console.log("Here5 " + this.switchToHexString);
            document.getElementById("payloadSwitch").innerText = this.switchToHexString;
//            console.log("Here6 " + document.getElementById("payloadSwitch").innerText);
        }
        sessionStorage.removeItem("aesKeyFormat");
        sessionStorage.removeItem("payloadFormat");
        }, 300);
        
    }
    
    
    postToServerDec() {
        console.log("Sending Request");
        
        this.finalAesKeyValue = this.aesKeyValue;
        this.finalPayloadValue = this.payloadValue;
        
        if (document.getElementById("aesKeySwitch").innerText == this.switchToHexString.toUpperCase()) {
            this.finalAesKeyValue = this.base64ToHex(this.finalAesKeyValue);
        }
        
        if (document.getElementById("payloadSwitch").innerText == this.switchToHexString.toUpperCase()) {
            this.finalPayloadValue = this.base64ToHex(this.finalPayloadValue);
        }
        
        let urlString = "https://guest-token-demo.herokuapp.com/d?" + "aes=" + this.finalAesKeyValue.toUpperCase() + "&"
                        + "payload=" + this.finalPayloadValue.toUpperCase();
                        
//        console.log("Aes " + this.aesKeyValue.toUpperCase());
//        console.log("Payload " + this.payloadValue.toUpperCase());

        console.log(urlString);
        this.http.get(urlString)
          .subscribe(data => {
            console.log(data);
            this.result = data;
            console.log(this.result);
    
           }, error => {
            console.log(error);
          });
    }
}
