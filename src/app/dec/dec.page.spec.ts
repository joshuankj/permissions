import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecPage } from './dec.page';

describe('DecPage', () => {
  let component: DecPage;
  let fixture: ComponentFixture<DecPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
