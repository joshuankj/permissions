import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncPage } from './enc.page';

describe('EncPage', () => {
  let component: EncPage;
  let fixture: ComponentFixture<EncPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
