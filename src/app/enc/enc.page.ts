import { Component, OnInit } from '@angular/core';
import { RequestOptions, Request, RequestMethod} from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-enc',
  templateUrl: './enc.page.html',
  styleUrls: ['./enc.page.scss'],
})

// npm run build -- --prod
export class EncPage implements OnInit {    
    
    switchToBase64String = "Switch to Base64";
    switchToHexString = "Switch to Hex";
    
    constructor(private http: HttpClient) {
        
    };
    
    hasClicked: Boolean = false;
    grid : Object;
    permissionsList = ["UNLOCK"]; // Unlock is checked by default
    startDateValue: string;
    endDateValue: string;
    accessRightValue: string;
    aesKeyValue: string;    
    
    finalAccessRightValue: string;
    finalAesKeyValue: string;
    
    result : Object;
    statusLabel: string;
    
    base64ToHex(base64String) {
          var raw = atob(base64String);

          var HEX = '';

          for (var i = 0; i < raw.length; i++ ) {

            var _hex = raw.charCodeAt(i).toString(16)

            HEX += (_hex.length==2?_hex:'0'+_hex);

          }
          return HEX.toUpperCase();
    }
    
    hexToBase64(hexString) {
        return btoa(hexString.match(/\w{2}/g).map(function(a) {
            return String.fromCharCode(parseInt(a, 16));
        }).join(""));
    }
    
    switchFormat(switchBtnID) {
        try {
                this.statusLabel = "";
                var switchBtn = document.getElementById(switchBtnID + "Switch");
                var inputField = (<HTMLInputElement>document.getElementById(switchBtnID + "Input"));
                var currString = inputField.value;
//                console.log("switchBtn = " + switchBtn + "inputField = " + inputField + " string = " + currString);
//                console.log("swtichBtnInnerText = " + switchBtn.innerText + " sw = " + this.switchToBase64String.toUpperCase());
                if (switchBtn.innerText == this.switchToBase64String.toUpperCase()) {
//                console.log("converting from hex to base 64")
                    if (currString != "") {
                        inputField.value = this.hexToBase64(currString);
                    }  
                    switchBtn.innerText = this.switchToHexString;
                } else {
                    if (currString != "") {
                        inputField.value = this.base64ToHex(currString);
                    } 
                    switchBtn.innerText = this.switchToBase64String;
                }
        } catch (e) {
            if (e instanceof TypeError) {
                this.statusLabel = switchBtnID + " Field is currently invalid, please enter a valid value before converting";
            } else if (e instanceof DOMException) {
                this.statusLabel = "Invalid encoding, please enter a valid value";
            } else {
                console.error(e);
            }
        }
    }

    ngOnInit() {
//        console.log("Loaded");
//        console.log(this.base64ToHex("VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIDEzIGxhenkgZG9ncy4="));
//        console.log(this.hexToBase64("54686520717569636B2062726F776E20666F78206A756D7073206F766572203133206C617A7920646F67732E"))
//        this.testFunc();
    };
    
    postToServer() {
//        console.log("Sending Request");
        var accessRightFormat = "Hex";
        var aesKeyFormat = "Hex";
        this.finalAccessRightValue = this.accessRightValue;
        this.finalAesKeyValue = this.aesKeyValue;
    
        if (document.getElementById("accessRightsSwitch").innerText == this.switchToHexString.toUpperCase()) {
            accessRightFormat = "Base64";
            this.finalAccessRightValue = this.base64ToHex(this.finalAccessRightValue);
        }
        
        if (document.getElementById("aesKeySwitch").innerText == this.switchToHexString.toUpperCase()) {
            aesKeyFormat = "Base64";
            this.finalAesKeyValue = this.base64ToHex(this.finalAesKeyValue);
        }
        
        let urlString = "https://guest-token-demo.herokuapp.com/e?" + "startDate=" + this.startDateValue + "&"
                        + "endDate=" + this.endDateValue + "&"
                        + "accessRight=" + this.finalAccessRightValue + "&"
                        + "adminAesKey=" + this.finalAesKeyValue;

        this.http.get(urlString)
          .subscribe(data => {
            console.log(data);
            this.result = data;
            sessionStorage.setItem("aesKey", this.aesKeyValue);
            sessionStorage.setItem("aesKeyFormat", aesKeyFormat);
            sessionStorage.setItem("payload", this.result["payload"]);
            sessionStorage.setItem("payloadFormat", accessRightFormat);
            console.log(this.result);
    
           }, error => {
            console.log(error);
          });
    };

    setPermissions(permissionsList) {
        const permissionsMap = [
         // access_right[0] bit 56 to 63
         { id: 21, name: 'UNLOCK', bit_position: 56 },
         { id: 25, name: 'SET_TIME', bit_position: 58 },
         { id: 27, name: 'GET_TIME', bit_position: 59 },
         { id: 29, name: 'GET_BATTERY_LEVEL', bit_position: 60 },
         { id: 31, name: 'GET_LOCK_STATUS', bit_position: 61 },
         { id: 33, name: 'SET_VOLUME', bit_position: 62 },
         { id: 35, name: 'RESET_LOCK', bit_position: 63 },
         // access_right[1] bit 48 to 55
         { id: 37, name: 'SET_AUTORELOCK', bit_position: 48 },
         { id: 39, name: 'SET_MAX_INCORRECT_PINS', bit_position: 49 },
         { id: 41, name: 'GET_LOGS', bit_position: 50 },
         { id: 43, name: 'CREATE_PIN', bit_position: 51 },
         { id: 45, name: 'EDIT_PIN', bit_position: 52 },
         { id: 47, name: 'DELETE_PIN', bit_position: 53 },
         { id: 49, name: 'SET_MASTER_PIN', bit_position: 54 },
         { id: 51, name: 'LOCK', bit_position: 55 },
         // access_right[2] bit 40 to 47
         { id: 55, name: 'ENABLE_AUTOUNLOCK', bit_position: 41 },
         { id: 57, name: 'BLACKLIST_GUEST_KEY', bit_position: 42 },
         { id: 59, name: 'UNBLACKLIST_GUEST_KEY', bit_position: 43 },
         { id: 61, name: 'ENABLE_DFU', bit_position: 44 },
         { id: 63, name: 'SET_DAYLIGHT_SAVINGS', bit_position: 45 },
         { id: 65, name: 'ADD_CARD', bit_position: 46 },
         { id: 67, name: 'DELETE_CARD', bit_position: 47 },
         // access_right[3] bit 32 to 39
         { id: 71, name: 'SET_BRIGHTNESS', bit_position: 33 },
        ];
        
        var resultPermissions = new Array(64).fill(0);
        
        // console.log("permissionMap");
        // console.log(permissionsMap);

        function validateAndSetPermissionsList() {
            try {
                for (var i = 0; i < permissionsList.length; i++)
                {
                    var p = permissionsList[i];
                    var found = false;
                    for (var j = 0; j < permissionsMap.length; j++) {
                        if (permissionsMap[j].name == p) {
                            found = true;
                            var bitIndex = permissionsMap[j].bit_position;
                            resultPermissions[63 - bitIndex] = 1;
                            break;
                        }
                    }
                    if (!found) {
                        // console.log("Permissions invalid");
                        throw "Invalid permission found: " + p;
                    }
                }
            } catch (e) {
                console.log(e);
            }
        };

        function convertToBytes() {
            var finalResult = "";
            for (var i = 0; i < resultPermissions.length; i+=4) {
                /*if ((i % 8) == 0 && i != 0) {
                    finalResult += " ";
                }*/
                var subString = resultPermissions.slice(i, i + 4).join("");
                var hex = parseInt(subString, 2).toString(16).toUpperCase();
                finalResult += hex;
            }

            return finalResult;
        }

        // Execute
        validateAndSetPermissionsList();
        console.log("Binary String: "  + resultPermissions.join(""))
        const finalByteResult = convertToBytes();
        console.log("Permissions Hex: " + finalByteResult);
        return finalByteResult;

    };
    
    updatePermissionsList(s) {
        // Disable the input field once a checkbox has been checked
        var element = <HTMLInputElement> document.getElementById('accessRightsInput');
        element.disabled = true;
        
        //console.log(this.permissionsList);
        if (this.permissionsList.indexOf(s) > -1) {
            let index = this.permissionsList.indexOf(s);
            this.permissionsList.splice(index, 1);
        } else {
            this.permissionsList.push(s);
            
        }
        this.accessRightValue = this.setPermissions(this.permissionsList);
        //console.log(this.permissionsList);
    };
    
    // TODO: Show or hide updates access rights accordingly
    // Inject the conversion functions globally

    showOrHideGrid() {
        if (document.getElementById('grid').style.display == "none") {
            document.getElementById('grid').style.display = "block";
            document.getElementById('showButton').innerText = "Hide";
        } else {
            document.getElementById('grid').style.display = "none";
            document.getElementById('showButton').innerText = "Show"; 
            
        }
    };
}
