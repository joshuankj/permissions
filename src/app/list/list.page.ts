import { Component } from '@angular/core';
import { RequestOptions, Request, RequestMethod} from '@angular/http';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss'],
})
export class ListPage {
    
    aesKeyValue: string;
    payloadValue: string;
    result;
    
    constructor(private http: HttpClient) {
    
    }


    ngOnInit() {
        console.log("Hi2");
    }

    log() {
        console.log("Hello World");
    }
    
    
    
    postToServerDec() {
        console.log("Sending Request");
        
        let urlString = "https://guest-token-demo.herokuapp.com/d?" + "aes=" + this.aesKeyValue.toUpperCase() + "&"
                        + "payload=" + this.payloadValue.toUpperCase();
                        
        console.log("Aes " + this.aesKeyValue.toUpperCase());
        console.log("Payload " + this.payloadValue.toUpperCase());

        console.log(urlString);
        this.http.get(urlString)
          .subscribe(data => {
            console.log(data);
            this.result = data;
            console.log(this.result);
    
           }, error => {
            console.log(error);
          });
    }
    
}
