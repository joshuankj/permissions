import { Component } from '@angular/core';
import { RequestOptions, Request, RequestMethod} from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
    
    startDateValue: string;
    endDateValue: string;
    accessRightValue: string;
    aesKeyValue: string = "fc3b6e4bfbb9a15be72cf55ad5240101";
    
    result;
    
    
    constructor(private http: HttpClient) {
    
    }


    ngOnInit() {
        console.log("Hi2");
    }

    log() {
        console.log("Hello World");
        console.log(this.startDateValue);
    }
    
    
    
    postToServer() {
        console.log("Sending Request");

        let postData = {
                "startDate": this.startDateValue,
                "endDate": this.endDateValue,
                "accessRight": this.accessRightValue,
                "adminAesKey": this.aesKeyValue
        }
        
        console.log(postData);
        
        let urlString = "https://guest-token-demo.herokuapp.com/e?" + "startDate=" + this.startDateValue + "&"
                        + "endDate=" + this.endDateValue + "&"
                        + "accessRight=" + this.accessRightValue + "&"
                        + "adminAesKey=" + this.aesKeyValue;

        this.http.get(urlString)
          .subscribe(data => {
            console.log(data);
            this.result = data;
            console.log(this.result);
    
           }, error => {
            console.log(error);
          });
    }
    
}
